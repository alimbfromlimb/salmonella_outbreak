import warnings
warnings.filterwarnings("ignore")

from Bio import SeqIO
from Bio import pairwise2
from Bio.pairwise2 import format_alignment

from tqdm import tqdm
from time import time
import copy
from collections import defaultdict

from matplotlib import pyplot as plt
import seaborn as sns
import scipy.stats as sps

import Levenshtein as lev

import sys

from Bio.Seq import Seq
from Bio.Blast import NCBIWWW
from Bio.Blast import NCBIXML


def calculate_patterns_frequency(records, k):
    """
    Given a list reads, the function calculates how often each
    k-mer of length k is present in the reads.

    input: a list of reads, k: int
    output: patterns: dict like {kmer: number of its occurences}
    """
    patterns = defaultdict(lambda: 0)

    for record in tqdm(records, total=1993167):
        for start_position in range(250 - k + 1):
            pattern = record[start_position: start_position + k]
            patterns[pattern] += 1

    return patterns


def calculate_patterns_frequency_with_origin(records, k):
    """
    Same as calculate_patterns_frequency, but also outputs a dictionary
    of the indices of the reads each fourth k-mer came from.
    This is needed for the algorithm evaluation using the blast database,
    as it can't process short chunks of genes.

    input: a list of reads, k: int
    output: (patterns: dict like {kmer: number of its occurences},
             origins: dict like {kmer: index of the source read})
    """
    patterns = defaultdict(lambda: 0)
    origins = defaultdict(lambda: None)

    count = 0

    for i, record in tqdm(enumerate(records), total=1993167):
        for start_position in range(250 - k + 1):
            pattern = record[start_position: start_position + k]
            patterns[pattern] += 1
            if pattern not in origins.keys() and count % 4 == 0:
                origins[pattern] = i
            count += 1

    return patterns, origins


def plot_for_k(k, patterns, plot_theoretical=False, x_lim=400):
    """
    A function which plots the histogram for a given dictionary,
    which contains the number of k-mer occurences.

    input: k: int
    patterns: a dictionary like {kmer: number of its occurences}
    plot_theoretical: bool, indicates whether to plot or not the
                      theoretical Poisson distribution expected
    x_lim: plot parameter
    """
    plt.figure(figsize=(25, 5))
    x = range(0, 200)
    sns.histplot(data=patterns.values(), binwidth=1)

    if plot_theoretical:
        # here we calculate the parameters of the Poisson distribution and
        # scale it
        N = 1993167
        L = 250
        G = 5 * (10 ** 6)
        p = (L - k + 1) / G
        mu = N * p
        coefficent = sum([v for k, v in patterns.items() if 25 < v and v < 75])
        plt.plot(x, sps.poisson.pmf(x, N * p) * coefficent / mu, color='green')

    plt.title(f'k = {k}', fontsize=20)
    plt.xlabel('Frequency', fontsize=20)
    plt.ylabel('Number of patterns', fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.xlim(0, x_lim)
    plt.grid()
    plt.tight_layout()

    plt.savefig(f'drive/MyDrive/computation_biology/pics/pic_{k}_{time()}.png')
    plt.show()


def build_sequences(patterns, thr=25):
    """
    The function builds a genetic sequence from the given kmers.

    inputs: patterns: dict like {'kmer': number of its occurences}
            thr: int, the frequency below which the kmers
            are considered as errors
    """
    result = None
    sequences = []

    for k, v in patterns.items():
        if v >= thr:
            if result is None:
                result = copy.copy(k)
            elif result[-13:] == k[:-1]:
                result += k[-1:]
            elif result[-12:] == k[:-2]:
                result += k[-2:]
            elif result[-11:] == k[:-3]:
                result += k[-3:]
            else:
                sequences.append(result)
                result = copy.copy(k)

    sequences.append(result)
    return sequences


def allign(w, m):
    """
    The function alligns two genetic strings, for better visualization.

    inputs: w and m – strings of wild and mutated genes
    ouput: a dictionary of alligned sequences like
           {'wild': wild sequence, 'mutated': mutated_sequence}
    """
    if w[:3] == m[:3]:
        return {'wild': w[:min(len(w), len(m))], 'mutated': m[:min(len(w), len(m))]}
    elif w[-3:] == m[-3:]:
        return {'wild': w[-min(len(w), len(m)):], 'mutated': m[-min(len(w), len(m)):]}
    else:
        return {'wild': w[:], 'mutated': m[:]}


def return_containing_seq(alligned_sequences_pair, mutated_origins, wild_origins,
                          mutated_dna_records, wild_dna_records, k=14):
    """
    Given as input a dictionary containing the mutated and wild sequences,
    returns the containing reads.
    """
    idx = None
    result = {}

    for i, letter in enumerate(alligned_sequences_pair['wild']):
        if alligned_sequences_pair['mutated'][i] != letter:
            idx = i
            break

    target = alligned_sequences_pair['mutated'][max(idx - k + 1, 0):
                                                min(idx + k, len(alligned_sequences_pair['mutated']))]

    for i in range(len(target)):
        kmer = target[i:i + k]
        n = mutated_origins[kmer]
        if n is not None:
            seq = mutated_dna_records[n]
            result['mutated'] = seq
            break

    ########

    target = alligned_sequences_pair['wild'][max(idx - k + 1, 0):
                                             min(idx + k, len(alligned_sequences_pair['wild']))]

    for i in range(len(target)):
        kmer = target[i:i + k]
        n = wild_origins[kmer]
        if n is not None:
            seq = wild_dna_records[n]
            result['wild'] = seq
            break

    return result


def print_mathed_reads(containing_reads):
    for seq in containing_reads:
        wild = seq['wild']
        mutated = seq['mutated']
        alignments = pairwise2.align.globalms(wild, mutated, 1, -1, -5, -5,
                                              one_alignment_only=True, penalize_end_gaps=False)

        for a in alignments:
            print(format_alignment(*a))


salmonella_enterica_wild_path = str(sys.argv[1])
salmonella_enterica_variant_path = str(sys.argv[2])

wild_dna_records = []
mutated_dna_records = []

# here one should use their own paths to the input files

print()
print()
print('Hello! You are using the code written for the Computational Biology project')
print('by the UGA students Anastasia Gorbunova, Alim Bukharaev and Adrien Deverin')
print()
print("Please feel free to contact us if you have any questions via")
print("bukharaev.an@phystech.edu")
print()
print()
print("Now we shall start...")
print()
print()
print('We first read the fasta files... (takes about a minute)')
print()

for seq_record in tqdm(SeqIO.parse(salmonella_enterica_wild_path, 'fasta'), total=1993167):
    wild_dna_records.append(str(seq_record.seq))

for seq_record in tqdm(SeqIO.parse(salmonella_enterica_variant_path, 'fasta'), total=1993167):
    mutated_dna_records.append(str(seq_record.seq))

k = 14
outliers_thr = 15

print()
print()

print('Now, we calculate the k-mers... (takes about 20 minutes)')
print('Please note that the program requires around 12GB of available RAM to run!')
print()

wild_patterns, wild_origins = calculate_patterns_frequency_with_origin(wild_dna_records, k)
filtered_wild = {k: v for k, v in wild_patterns.items() if v > outliers_thr}
del wild_patterns

mutated_patterns, mutated_origins = calculate_patterns_frequency_with_origin(mutated_dna_records, k)
filtered_mutated = {k: v for k, v in mutated_patterns.items() if v > outliers_thr}
del mutated_patterns

wild_not_mutated_dict = {k: v for k, v in filtered_wild.items() if k not in filtered_mutated.keys()}
mutated_not_wild_dict = {k: v for k, v in filtered_mutated.items() if k not in filtered_wild.keys()}

wild_sequences = build_sequences(wild_not_mutated_dict)
mutated_sequences = build_sequences(mutated_not_wild_dict)

print()
print()
print('Building alligned sequences from the k-mers...')
print()
print()
print()

print('And here are the results:')
print()

alligned_sequences = []

# let's fill the the array of matched sequences
for w in wild_sequences:
    for m in mutated_sequences:
        if lev.distance(w, m) <= 5:
            pair = allign(w, m)
            print('wild')
            print(pair['wild'])
            print()
            print('mutated')
            print(pair['mutated'])
            print()
            print('---------------------------')
            print()
            alligned_sequences.append(pair)

print('You can clearly see the forward and the reverse sequences and the mutations (TTT -> GGG and AAA -> CCC)')
print()
print('Now let\'s validate the results.')
print('We will look through the BLAST database to see what we have found:')


containing_reads = [return_containing_seq(alligned_sequences[0], mutated_origins,
                          wild_origins, mutated_dna_records, wild_dna_records),
                   return_containing_seq(alligned_sequences[1], mutated_origins,
                          wild_origins, mutated_dna_records, wild_dna_records)]

print()
print()
print('The resulting sequences are too short to be identified by BLAST.')
print('so we first retrieve the original reads from which the sequences came')

print_mathed_reads(containing_reads)

print()
print()

print("Now, we will search through the BLAST dataset and see what we get...")

read = containing_reads[1]['mutated']
my_seq = Seq(read)

result_handle = NCBIWWW.qblast("blastx", "nr", my_seq, entrez_query='Salmonella enterica[organism]')
blast_record = NCBIXML.read(result_handle)
result_handle.close()

for alignment in blast_record.alignments:
    for hsp in alignment.hsps:
        print(alignment.title)
        break
    break

print()
print()
print("Hurray! We got the TetR family transcriptional regulator!")
print()
print("Thank you for using our program!")
print("Once again, please feel free to contact us in case you have any questions or looking for cooperation :)")
print()
