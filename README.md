# salmonella_outbreak

Project on Computational biology done by Ensimag students Anastasiia Gorbunova, Alim Bukharaev and Adrien Deverin

As a disastrous salmonella outbreak is currently happening all over the world, millions of people die everyday, and antibiotics do not help at all. For that reason, it is supposed that some mutation has happened in the salmonella DNA, which should be detected and understood. Fortunately, one talented biologist has managed to obtain two DNA strands: one of the resistant to antibiotics mutant, and another one of the wild salmonella. 

Playing a role of the consulting team, we managed to provide TATFAR with a straightforward solution. Our code works in linear time, simple to utilize and makes use of Biopython library. The obtained results were successfully validated using the BLAST database. We hope that our employer was fully satisfied with our work and we will be payed as it was detailed in the estimation of the costs we sent to the company previously. We also hope, of course, that it will help the TATFAR team to develop a new antibiotic which would stop the disastrous outspread of the variant bacteria.

